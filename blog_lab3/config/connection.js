const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'bloghost'
});

connection.connect();

//j'exporte cette connection pour pouvoir la reutiliser partout ou jaurai besoin
module.exports = connection