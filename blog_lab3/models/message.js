
let connection = require('../config/connection')
let moment = require('moment')

class Message{

    constructor (row){
        this.row = row
    }

    get Contenu(){
        return this.row.Contenu
    }

    get laDate(){
        return moment(this.row.laDate)
    }

    get lapersonne(){
        return this.row.lapersonne
    }

    static create(content, laperson, cb){
        connection.query('INSERT INTO commentaires SET Contenu = ?, laDate = ?, lapersonne=?', [content, new Date(), laperson], (err, resultat) => {
            if(err) throw err
            cb(resultat)
        }) 
    }

    static all(cb){
        connection.query('SELECT * FROM commentaires', (err, rows) => {
            if(err) throw err
            cb(rows.map((row) => new Message(row)))
        })
    }

    static sendTo(email, content, cb){
        connection.query('INSERT INTO contact SET email=?, message=?', [email, content], (err, resultat) => {
            if(err) throw err
            cb(resultat)
        })
    }
    
}

module.exports = Message