const express = require('express')
const app = express()
const multer = require('multer')
const ejs = require('ejs')
const bodyParser = require('body-parser')
const port = 9000
const session = require('express-session')

//EJS
app.set('view engine', 'ejs')

//static folder
app.use('/assets', express.static('public'))

//middlewares : traitement de routes => body-parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//fonctionnalites des sessions: Depot github
app.use(session({
    secret: 'mesErreurs',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false } //on ne traite pas la https
  }))

app.use(require('./middlewares/flash'))

let ssn

//-------GETS-----------------------------
app.get('/', (request, response) => {
    let Message = require('./models/message')
    Message.all(function(messages){
        response.render('index', {messages : messages})
    })
})

app.get('/about', (request, response) =>{
    response.render('about')
})

app.get('/contact', (request, response) =>{
    response.render('contact')
})

app.get('/login', (request, response) => {
    response.render('login')
})
//------------------------------------------


//------POSTS-------------------------------

app.post('/addComment', (request, response) => {
    //Gerer le vide : on ne doit pas avoir un commentaire vide
    if(request.body.myComment === undefined || request.body.myComment === '')
    {
        response.send("RIEN")
        //request.flash('error', "Vous n'avez rien saisi!")
        //response.redirect('/')
    }
    else{
        let Message = require('./models/message')
        Message.create(request.body.myComment, ssn.nom, () => {
            //request.flash('success', "Merci!")
            //response.redirect('/')
            response.send("OK")
        })
        
    }
})

app.post('/sending', (request, response) => {
    //Gerer le vide : on ne doit pas avoir un commentaire vide
    if(request.body.customer_email === undefined || request.body.customer_email === '' || request.body.customer_message==='')
    {
        response.render('failed')
    }
    else{
        let Message = require('./models/message')
        Message.sendTo(request.body.customer_email, request.body.customer_message, () => {
            response.render('completed')
        })
        
    }
})


app.post('/login', (request, response) => {
    if(request.body.user ==='' || request.body.pass ===''){
        response.render('failed')
    }
    else{
        let connection = require('./config/connection')
        connection.query(`SELECT * FROM users WHERE username='${request.body.user}' and password='${request.body.pass}'`, (err, rows) => {
            if(err) throw err
            ssn = request.session
            ssn.nom = rows[0].name
            //console.log(ssn.nom)

            let Message = require('./models/message')
            Message.all(function(messages){
            response.render('index', {messages : messages})
            //console.log(rows[3].name)
            })
        })
    }
})

//------------------------------------------
app.listen(port, () => {
    console.log(`Server listens to port : ${port}`)
})

