module.exports = (request, response, next) => {
    
    //definir les messages d'erreurs sur le systemes local : pour qu'ils puissent s'afficher sur la vue
    if(request.session.flash){
        response.locals.flash = request.session.flash
        request.session.flash = undefined
    }

    request.flash = (type, content) => {
        //cette fonction stockera dans la session le message d'erreur
        if(request.session.flash === undefined)
        {
            //si la session n'existe pas encore : on stocke un message vide --> pour eviter les problemes futurs
            request.session.flash = {}
        }
               //une cle flash qui va contenir l'erreur
            request.session.flash[type] = content
    }


        next()
    }
    
    